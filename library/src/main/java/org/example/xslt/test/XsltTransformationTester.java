package org.example.xslt.test;

import static org.junit.Assert.assertFalse;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStreamWriter;
import java.net.URISyntaxException;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import javax.xml.XMLConstants;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.Source;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerConfigurationException;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.TransformerFactoryConfigurationError;
import javax.xml.transform.URIResolver;
import javax.xml.transform.sax.SAXSource;
import javax.xml.transform.stream.StreamResult;
import javax.xml.transform.stream.StreamSource;
import javax.xml.validation.Schema;
import javax.xml.validation.SchemaFactory;
import javax.xml.xpath.XPathExpressionException;
import javax.xml.xpath.XPathFactory;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.junit.runners.Parameterized.Parameter;
import org.springframework.core.io.ClassPathResource;
import org.springframework.core.io.DefaultResourceLoader;
import org.springframework.core.io.Resource;
import org.w3c.dom.Document;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;
import org.xml.sax.XMLReader;
import org.xml.sax.helpers.XMLReaderFactory;
import org.xmlunit.builder.DiffBuilder;
import org.xmlunit.diff.Comparison;
import org.xmlunit.diff.ComparisonResult;
import org.xmlunit.diff.Diff;
import org.xmlunit.diff.DifferenceEvaluator;
import org.xmlunit.diff.DifferenceEvaluators;
import org.xmlunit.validation.JAXPValidator;
import org.xmlunit.validation.Languages;
import org.xmlunit.validation.ValidationProblem;
import org.xmlunit.validation.ValidationResult;
import org.xmlunit.validation.Validator;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@RunWith(Parameterized.class)
public abstract class XsltTransformationTester {

    public static class ClasspathAbleURIResolver implements URIResolver {

        private final DefaultResourceLoader defaultResourceLoader = new DefaultResourceLoader();

        @Override
        public Source resolve(final String href, final String base) throws TransformerException {
            Source source = null;
            final Resource resource = defaultResourceLoader.getResource(href);

            try {
                source = new StreamSource(resource.getInputStream());
            } catch (final IOException e) {
                throw new TransformerException(e);
            }
            return source;
        }
    }

    private Transformer xsltTransformer;
    private Schema sourceSchema;
    private Schema resultSchema;
    private Validator resultDTDValidator;
    private Validator sourceDTDValidator;
    private boolean shouldValidateExpectedResultUsingDTD = false;

    private final Map<String, String> xsltParameters = new HashMap<>();
    private final Set<String> xpathsToIgnore = new HashSet<>();

    @Parameter(0)
    public String sourceXmlFileName;

    @Parameter(1)
    public String expectedResultXmlFileName;

    protected void setSourceDTDValidatingTrue() throws URISyntaxException {
        sourceDTDValidator = Validator.forLanguage(Languages.XML_DTD_NS_URI);
    }

    protected void setResultDTDFileName(final String resultDTDFileName) throws URISyntaxException {
        final StreamSource streamSource = new StreamSource(XsltTransformationTester.class.getClassLoader().getResourceAsStream(resultDTDFileName));
        streamSource.setSystemId(XsltTransformationTester.class.getClassLoader().getResource(resultDTDFileName).toURI().toString());
        resultDTDValidator = Validator.forLanguage(Languages.XML_DTD_NS_URI);
        resultDTDValidator.setSchemaSource(streamSource);
    }

    private Schema validateAndCreateSchemaFromFilename(final String xmlSchemaFileName) {
        final Resource schema = new ClassPathResource(xmlSchemaFileName);

        if (!schema.exists()) {
            throw new IllegalStateException("the XML Schema " + xmlSchemaFileName + " does not exists");
        }

        try {
            final InputSource inputSource = new InputSource(schema.getInputStream());
            inputSource.setSystemId(schema.getURI().toString());

            final XMLReader xmlReader = XMLReaderFactory.createXMLReader();
            xmlReader.setFeature("http://xml.org/sax/features/namespace-prefixes", true);

            final SchemaFactory schemaFactory = SchemaFactory.newInstance(XMLConstants.W3C_XML_SCHEMA_NS_URI);
            return schemaFactory.newSchema(new SAXSource(xmlReader, inputSource));

        } catch (IOException | SAXException e) {
            throw new IllegalStateException(e);
        }
    }

    protected void setResultSchema(final String aResultSchemaFileName) {
        resultSchema = validateAndCreateSchemaFromFilename(aResultSchemaFileName);
    }

    protected void setSourceSchema(final String aSourceSchemaFileName) {
        sourceSchema = validateAndCreateSchemaFromFilename(aSourceSchemaFileName);
    }

    protected void setXsltFileName(final String xsltFileName) throws TransformerConfigurationException, TransformerFactoryConfigurationError,
                    ParserConfigurationException, SAXException, IOException, XPathExpressionException {

        final TransformerFactory transformerFactory = TransformerFactory.newInstance();
        transformerFactory.setURIResolver(new ClasspathAbleURIResolver());
        xsltTransformer = transformerFactory.newTransformer(new StreamSource(XsltTransformationTester.class.getClassLoader().getResourceAsStream(xsltFileName)));
        final DocumentBuilder documentBuilder = DocumentBuilderFactory.newInstance().newDocumentBuilder();
        final Document document = documentBuilder.parse(XsltTransformationTester.class.getClassLoader().getResourceAsStream(xsltFileName));
        shouldValidateExpectedResultUsingDTD = XPathFactory.newInstance().newXPath()
                        .evaluate("/*[local-name()='stylesheet']/*[local-name()='output']/@doctype-system", document).trim().length() > 0;
    }

    protected void addXsltParameter(final String name, final String value) {
        xsltParameters.put(name, value);
    }

    protected void addXPathToIgnore(final String aXPathToIgnore) {
        xpathsToIgnore.add(aXPathToIgnore);
    }

    private void validateSourceXmlUsingDTD(final String xmlFileName) throws IOException {
        try (final InputStream inputStream = getClass().getClassLoader().getResourceAsStream(xmlFileName)) {
            final ValidationResult result = sourceDTDValidator.validateInstance(new StreamSource(inputStream));

            if (!result.isValid()) {
                final StringBuilder errorMessage = new StringBuilder(xmlFileName + ": invalid source XML according to DTD: ");

                for (final ValidationProblem problem : result.getProblems()) {
                    errorMessage.append(problem.getMessage());
                }
                throw new IllegalStateException(errorMessage.toString());
            }
        }
    }

    private void validateResultXmlUsingDTD(final InputStream inputStream, final String xmlFileName) throws IOException {
        final ValidationResult result = resultDTDValidator.validateInstance(new StreamSource(inputStream));
        inputStream.close();

        if (!result.isValid()) {
            final StringBuilder errorMessage = new StringBuilder(xmlFileName + ": invalid result XML according to DTD: ");

            for (final ValidationProblem problem : result.getProblems()) {
                errorMessage.append(problem.getMessage());
            }
            throw new IllegalStateException(errorMessage.toString());
        }
    }

    private void validateSourceXmlUsingXMLSchema(final String xmlFileName) throws IOException {
        final Validator validator = Validator.forLanguage(Languages.W3C_XML_SCHEMA_NS_URI);
        ((JAXPValidator) validator).setSchema(sourceSchema);

        try (final InputStream inputStream = getClass().getClassLoader().getResourceAsStream(xmlFileName)) {
            final ValidationResult result = validator.validateInstance(new StreamSource(inputStream));

            if (!result.isValid()) {
                final StringBuilder errorMessage = new StringBuilder(xmlFileName + ": invalid source XML according to schema: ");

                for (final ValidationProblem problem : result.getProblems()) {
                    errorMessage.append(problem.getMessage());
                }
                throw new IllegalStateException(errorMessage.toString());
            }
        }
    }

    private void validateResultXmlUsingXMLSchema(final InputStream inputStream, final String xmlFileName) throws IOException {
        final Validator validator = Validator.forLanguage(Languages.W3C_XML_SCHEMA_NS_URI);
        ((JAXPValidator) validator).setSchema(resultSchema);
        final ValidationResult result = validator.validateInstance(new StreamSource(inputStream));
        inputStream.close();

        if (!result.isValid()) {
            final StringBuilder errorMessage = new StringBuilder(xmlFileName + ": invalid result XML according to schema: ");

            for (final ValidationProblem problem : result.getProblems()) {
                errorMessage.append(problem.getMessage());
            }
            throw new IllegalStateException(errorMessage.toString());
        }
    }

    @Test
    public void shouldSuccessfullyTransformSourceToResult() throws IOException, TransformerException {

        if (null != sourceSchema) {
            validateSourceXmlUsingXMLSchema(sourceXmlFileName);
        } else {
            if (null != sourceDTDValidator) {
                validateSourceXmlUsingDTD(sourceXmlFileName);
            }
        }

        if (null != resultSchema) {
            validateResultXmlUsingXMLSchema(getClass().getClassLoader().getResourceAsStream(expectedResultXmlFileName), expectedResultXmlFileName);
        } else {
            if (null != resultDTDValidator) {
                validateResultXmlUsingDTD(getClass().getClassLoader().getResourceAsStream(expectedResultXmlFileName), expectedResultXmlFileName);
            }
        }

        try (final InputStream inputStream = getClass().getClassLoader().getResourceAsStream(sourceXmlFileName)) {
            final StreamSource sourceXml = new StreamSource(inputStream);
            final File tempFile = File.createTempFile(expectedResultXmlFileName.replaceAll("\\.xml$", ""), ".xml", new File("target"));

            try (final OutputStreamWriter transformedResult = new OutputStreamWriter(new FileOutputStream(tempFile),
                            Charset.forName(StandardCharsets.UTF_8.name()))) {

                for (final String key : xsltParameters.keySet()) {
                    xsltTransformer.setParameter(key, xsltParameters.get(key));
                }
                xsltTransformer.transform(sourceXml, new StreamResult(transformedResult));
            }
            inputStream.close();
            log.info("XSLT transformed result available in: {}", tempFile.getAbsolutePath());

            if (null != resultSchema) {
                validateResultXmlUsingXMLSchema(new FileInputStream(tempFile), tempFile.getAbsolutePath());
            } else {
                if (null != resultDTDValidator && shouldValidateExpectedResultUsingDTD) {
                    validateResultXmlUsingDTD(new FileInputStream(tempFile), tempFile.getAbsolutePath());
                }
            }

            try (final InputStream expectedResultXml = getClass().getClassLoader().getResourceAsStream(expectedResultXmlFileName)) {
                final Diff diff = DiffBuilder.compare(expectedResultXml).withTest(tempFile).checkForSimilar().ignoreWhitespace().ignoreComments()
                                .normalizeWhitespace().withDifferenceEvaluator(DifferenceEvaluators.chain(DifferenceEvaluators.Default, new DifferenceEvaluator() {

                                    @Override
                                    public ComparisonResult evaluate(final Comparison comparison, final ComparisonResult outcome) {

                                        if (ComparisonResult.EQUAL != outcome) {

                                            for (final String aXPathToIgnore : xpathsToIgnore) {
                                                if (comparison.getTestDetails().getXPath().equals(aXPathToIgnore)) {
                                                    return ComparisonResult.EQUAL;
                                                }
                                            }
                                        }
                                        return outcome;
                                    }

                                })).build();
                assertFalse("XML not similar, " + diff.toString(), diff.hasDifferences());
            }
        }
    }
}