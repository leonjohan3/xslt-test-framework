package org.example.xslt;

import java.io.IOException;
import java.net.URISyntaxException;
import java.util.Arrays;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.TransformerConfigurationException;
import javax.xml.transform.TransformerFactoryConfigurationError;
import javax.xml.xpath.XPathExpressionException;

import org.example.xslt.test.XsltTransformationTester;
import org.junit.Before;
import org.junit.runners.Parameterized.Parameters;
import org.xml.sax.SAXException;

public class MyXSLTTransformTest extends XsltTransformationTester {

    @Before
    public void beforeRunningEachTest() throws TransformerConfigurationException, TransformerFactoryConfigurationError, URISyntaxException,
                    XPathExpressionException, ParserConfigurationException, SAXException, IOException {

        // Set the XSLT source document and optionally the XML source and expected
        // result DTDs or XML Schemas (XML Schemas override DTDs if both are set).
        setXsltFileName("org/example/xslt/test/example-for-schema.xsl");
        setSourceSchema("org/example/xslt/test/example.xsd");
        setResultSchema("org/example/xslt/test/result.xsd");

        // Optionally set parameters that might be required by the XSLT source document.
        addXsltParameter("one", "today");
        addXsltParameter("unused", "dummy");

        // Optionally set XPaths to ignore in the comparison between the transformed
        // result XML and the expected result XML.
        addXPathToIgnore("/menuSummary[1]/@now");
        addXPathToIgnore("/menuSummary[1]/totalPrice[1]/text()[1]");
    }

    @Parameters
    public static Iterable<Object[]> data() {

        // Below shows how to use multiple scenarios/test cases in a single test class.
        return Arrays.asList(new Object[][] {
                        /*
                         * { "org/example/xslt/test/another-commented-scenario-source.xml",
                         * "org/example/xslt/test/another-commented-scenario-result.xml" },
                         */
                        { "org/example/xslt/test/example-using-xml-schema.xml", "org/example/xslt/test/result-using-xml-schema.xml" } });
    }
}
