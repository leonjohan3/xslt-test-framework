package org.example.xslt;

import static org.hamcrest.CoreMatchers.containsString;
import static org.hamcrest.CoreMatchers.instanceOf;
import static org.hamcrest.CoreMatchers.notNullValue;
import static org.hamcrest.Matchers.greaterThan;
import static org.junit.Assert.assertThat;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;

import javax.xml.transform.Source;
import javax.xml.transform.TransformerException;
import javax.xml.transform.URIResolver;
import javax.xml.transform.stream.StreamSource;

import org.example.xslt.test.XsltTransformationTester;
import org.hamcrest.core.IsInstanceOf;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.springframework.core.io.ResourceLoader;

public class ClasspathAbleURIResolverTest {

    private final ExpectedException expectedException = ExpectedException.none();

    @Rule
    public ExpectedException getExpectedExceptionRule() {
        return expectedException;
    }

    @Test
    public void shouldResolveResourceFromClasspath() throws TransformerException, IOException {

        // given: all data (test fixture) preparation
        final URIResolver aURIResolver = new XsltTransformationTester.ClasspathAbleURIResolver();

        // when : method to be checked invocation
        final Source source = aURIResolver.resolve(ResourceLoader.CLASSPATH_URL_PREFIX + "org/example/xslt/test/shared-functions.xsl", "");

        // then : checks and assertions
        assertThat(source, notNullValue());
        assertThat(source, instanceOf(StreamSource.class));
        final StreamSource streamSource = (StreamSource) source;

        try (final InputStream inputStream = streamSource.getInputStream()) {
            assertThat(inputStream, notNullValue());
            assertThat(inputStream.available(), greaterThan(0));
        }
    }

    @Test
    public void shouldResolveResourceFromFile() throws TransformerException, IOException {

        // given: all data (test fixture) preparation
        final URIResolver aURIResolver = new XsltTransformationTester.ClasspathAbleURIResolver();

        // when : method to be checked invocation
        final Source source = aURIResolver.resolve("file:src/test/resources/org/example/xslt/test/shared-functions.xsl", "");

        // then : checks and assertions
        assertThat(source, notNullValue());
        assertThat(source, instanceOf(StreamSource.class));
        final StreamSource streamSource = (StreamSource) source;

        try (final InputStream inputStream = streamSource.getInputStream()) {
            assertThat(inputStream, notNullValue());
            assertThat(inputStream.available(), greaterThan(0));
        }
    }

    @Test
    public void shouldThrowExceptionWhenUnableToResolveResource() throws TransformerException {

        // given: all data (test fixture) preparation
        final URIResolver aURIResolver = new XsltTransformationTester.ClasspathAbleURIResolver();

        // then : checks and assertions
        expectedException.expect(TransformerException.class);
        expectedException.expectCause(IsInstanceOf.<Throwable>instanceOf(FileNotFoundException.class));
        expectedException.expectMessage(containsString("est/shared-functionss.xsl] cannot be opened because it does not exist"));

        // when : method to be checked invocation
        aURIResolver.resolve(ResourceLoader.CLASSPATH_URL_PREFIX + "org/example/xslt/test/shared-functionss.xsl", "");
    }
}