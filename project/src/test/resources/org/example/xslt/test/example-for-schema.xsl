<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="2.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:in="http://example.org/xslt/test/in"
    xmlns="http://example.org/xslt/test/out" xmlns:fn-example="http://example.org/xslt/test/fn-example" exclude-result-prefixes="in fn-example">

    <xsl:import href="classpath:org/example/xslt/test/shared-functions.xsl" />
    <xsl:output byte-order-mark="no" encoding="utf-8" indent="yes" method="xml" />

    <xsl:param name="one" />

    <xsl:template match="/in:menu">
        <menuSummary then="{$one}"
            now="{concat(format-date(current-date(), '[Y0001]-[M01]-[D01]'), 'T', format-time(current-time(), '[H01]:[m01]:[s01]'))}">
            <xsl:apply-templates select="in:foodItem" />

            <totalPrice>
                <xsl:value-of select="sum(in:foodItem/in:price)" />
            </totalPrice>
        </menuSummary>
    </xsl:template>

    <xsl:template match="in:foodItem">
        <xsl:if test="fn-example:isPresent(in:name)">
            <foodItem name="{in:name}" />
        </xsl:if>
    </xsl:template>

</xsl:stylesheet>