<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="2.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">

    <xsl:output byte-order-mark="no" doctype-system="test-classes/org/example/xslt/test/result.dtd" encoding="utf-8" indent="yes" method="xml" />

    <xsl:template match="/menu">

        <menuSummary
            now="{concat(format-date(current-date(), '[Y0001]-[M01]-[D01]'), 'T', format-time(current-time(), '[H01]:[m01]:[s01]'))}">
            <xsl:apply-templates select="foodItem" />

            <totalPrice>
                <xsl:value-of select="sum(foodItem/price)" />
            </totalPrice>
        </menuSummary>
    </xsl:template>

    <xsl:template match="foodItem">
        <foodItem name="{name}" />
    </xsl:template>

</xsl:stylesheet>