<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="2.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:fn-example="http://example.org/xslt/test/fn-example"
    xmlns:xsd="http://www.w3.org/2001/XMLSchema" exclude-result-prefixes="fn-example">

    <xsl:function name="fn-example:isPresent" as="xsd:boolean">
        <xsl:param name="valueToTest" />
        <xsl:value-of select="$valueToTest and normalize-space($valueToTest) != ''" />
    </xsl:function>

</xsl:stylesheet>