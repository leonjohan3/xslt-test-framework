# Overview
This is a project that demonstrates one way of creating and using a XSLT testing framework.

# What is it useful for?
If a project uses a significant amount of XSLT functionality, a XSLT testing framework will be essential in ensuring that the XSLT code does what it is supposed to do. It transforms source XML into a result XML/HTML/text using a supplied XSLT, then compares the transformed result with the expected result, and asserts that they are similar.

# So it is a XSLT unit test framework?
Yes, it assists with the creation and execution of test cases/scenarios for your XSLT source code. It provides a simplified interface for testing XSLT hiding the complexities of validating, transforming and comparing XML documents, thus improving developer productivity.

# What features does it support?
* It allows you to specify an input XML file, the XSLT source code, and the file containing the expected transformed output.
* Optionally (but recommended) the source, expected result and actual transformation output can, as part of the test, be validated using either a XML Schema or a DTD.
* The ability to provide parameters for the XSLT.
* The ability to specify XPaths to ignore certain differences, e.g. ignoring a XSLT generated XML tag that contains the current date or time.

# How do I use it?
1.  This project shows how a test library is used in a project that needs a XSLT test framework. Browse the files and make sure to understand the technologies involved.
2.  Copy the [XsltTransformationTester](https://bitbucket.org/leonjohan3/xslt-test-framework/src/master/library/src/main/java/org/example/xslt/test/XsltTransformationTester.java) class into your project or your own test library.
3.  Include the required dependencies according to the pom.xml files.
4.  Extend your own test class from the XsltTransformationTester class, see the [MyXSLTTransformTest](https://bitbucket.org/leonjohan3/xslt-test-framework/src/master/project/src/test/java/org/example/xslt/MyXSLTTransformTest.java) class as an example of how this could be done.

# What else should I keep in mind?
* This functionality will not work 100% for all scenarios or projects. For this reason it has not been packaged as a re-usable library but rather as a Java class that can be copied and adapted. It should work unmodified for most projects.
* Some fine tuning of the functionality that identifies the differences in the transformed result XML and the expected result XML might be required. For help on this, see the XMLUnit user guide https://github.com/xmlunit/user-guide/wiki.
* This functionality is designed to work with documents (DTDs, XML Schemas, XMLs, XSLTs) that are available in the classpath. This allows you to have these documents spread over numerous JARs and listed as Maven dependencies. This ensures that the tests are self-contained and not dependent on some webserver being available to serve e.g. a required XML Schema, or DTD, etc.
* The JUnit Parameterized runner is used to enable the testing of multiple business/technical scenarios/test cases in a single test class.
* When using DTDs, for the transformer and XMLUnit to work correctly, make sure the DOCTYPE in the XML references a DTD that is available in the file path. If this is not possible, an empty DTD in the appropriate folder will eliminate the problem. See the examples used. If possible, convert the DTDs to XML Schemas, and rather use them to ensure the XML is valid. DTDs are generally troublesome and therefore been replaced by XML Schemas.

# Resources
* https://en.wikipedia.org/wiki/Document_type_definition 
* https://en.wikipedia.org/wiki/XML_schema
* https://en.wikipedia.org/wiki/Java_API_for_XML_Processing 
* https://www.xml.com/articles/2017/02/14/why-you-should-be-using-xslt-30/ 
* https://stackoverflow.com/questions/1544200/what-is-difference-between-xml-schema-and-dtd 
* https://github.com/xmlunit/user-guide/wiki

# Tags
Java, Maven, JUnit, XMLUnit, XML, XSLT, DTD, XML Schema, XPath, Saxon-HE, [Markdown](https://bitbucket.org/tutorials/markdowndemo).